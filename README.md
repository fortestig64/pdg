# Python Docker Gitlab

## Task

Create a simple Python server, add some unit tests, and prepare a Docker image to be able to run it as a container. Automate this process with Gitlab CI/CD.

### Python

**Task 1:**

Create a simple REST API server. It can be any framework (Flask, Django, FastApi) that will serve the following requests:

- `/` - returns a message `"Hi, thank you for using this great REST API server.\n For more information send a GET request to /help/ endpoint."` 
- `/help/` - returns a `json` response with a list of all available commands

**Extra tasks, when the whole interview scope will be ready:**

- `/re/find/` - sends a request with `json` data: `{"string": "original string", "pattern": "pattern to find", "count": "'all' - by default, possible value - 'first'/'last'"}`. Response data is expected in `json` format with a list of strings matching the pattern, if `count=='all'` every match is returned, if `count=='first'` then only the first one if `count=='last'` then only the last one.  
- `/re/split/` - sends a request with `json` data : `{"string": "original string", "pattern": "split pattern", "count": "'all' - by default, possible value - 'first'/'last'"}`. Response data in `json` format with a list of strings split by pattern, if `count=='all'` every match is returned, if `count=='first'` then only the first one if `count=='last'` then only the last one. 
- `/re/sub/` - sends a request with `json` data : `{"string": "original string", "pattern": "patterns to replace", "replace": "string to replace pattern with", "count": "'all' - by default, possible value - 'first'/'last'"}`. Response data in `json` format with a modified string. Modify operation is a replacement of `pattern` parts of `string` with `replace`, if `count=='all'` every match is returned, if `count=='first'` then only the first one if `count=='last'` then only the last one. 


**Task 2:**

Create unit tests for all endpoints

**Extra tasks, when the whole interview scope will be ready:**

Add unit tests for added requests 

### Docker

Create a `Dockerfile` that describes an image with the Python project described above.

**Task 1:**

Create a `Dockerfile`, run a container to make it available for serving requests, and verify its correct execution.

**Task 2:**

Run several example `curl` requests to ensure that it works fine.

### Gitlab

**Task 1:**

Prepare a `.gitlab-ci.yml` file for future Gitlab CI/CD process. It have to contain 3 stages. Every stage requires the previous one to be successful:

- `build`: stage where a `dev` image is being built. Every triggered pipeline has to have its own historic image (tag).
- `test`: testing stage divided into two parts: 
  - Python unit tests
  - A container is deployed and some tests are run towards it
- `deploy`: manual job. If triggered then saves current docker image as `latest` in repository.


## System requirements
- Python: 3.8+
- Docker: basically any base image, but look into minimal setup images (smaller size)
- Gitlab CI/CD: docker executor